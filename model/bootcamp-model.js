const mongoose = require("mongoose");
const bootcampSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "name is required"],
    unique: true,
    trim: true,
    maxLength: [50, "cannot name exceed more than 50 character"],
  },
  slug: String,
  description: {
    type: String,
    required: [true, "description is required"],
    maxLength: [500, "cannot description exceed more than 50 character"],
  },
  website: {
    type: String,
  },
  email: {
    type: String,
    match: [
      /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
    ],
  },
  address: {
    type: String,
    required: [true, "address is required"],
  },
  location: {
    type: String,
    required: [true, "location is required"],
  },
  coordinate: {
    type: String,
    required: [true, "coordinate is required"],
  },
  formatedAddress: String,
  street: String,
  state: String,
  country: String,
  pincode: Number,
  careers: {
    type: [String],
    required: true,
    enum: ["web dev", "mobile dev"],
  },
});

module.exports = mongoose.model("bootcamp", bootcampSchema);
