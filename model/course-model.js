const mongoose = require("mongoose");

const courseSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "name is required"],
    trim: true,
  },
  description: {
    type: String,
    required: [true, "description is required"],
    maxLength: [500, "cannot description exceed more than 50 character"],
  },
  weeks: {
    type: String,
    required: [true, "weeks is required"],
  },
  tution: {
    type: Number,
    required: [true, "tution is required"],
  },
  min: {
    type: String,
    required: [true, "min is required"],
  },
  bootcamp: {
    type: mongoose.Schema.ObjectId,
    ref: "bootcamp",
    required: true,
  },
});
module.exports = mongoose.model("course", courseSchema);
