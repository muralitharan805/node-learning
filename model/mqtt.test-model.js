const mongoose = require("mongoose");
const mqttTestSchema = mongoose.Schema({
  data: String,
});
module.exports = mongoose.model("mqttTestModel", mqttTestSchema);
