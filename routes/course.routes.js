const express = require("express");
const {
  getAllCourse,
  createCourse,
} = require("../controllers/course-controller");
const courseRoutes = express.Router();

courseRoutes.route("/").get(getAllCourse).post(createCourse);

module.exports = courseRoutes;
