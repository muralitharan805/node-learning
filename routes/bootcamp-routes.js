const express = require("express");
const {
  getAllBootcamp,
  getSpecificeBootcamp,
  createBootcamp,
  updateBootcamp,
  deleteBootcamp,
} = require("../controllers/bootcamp-controller");
const router = express.Router();

router.route("/").get(getAllBootcamp).post(createBootcamp);

router
  .route("/:id")
  .get(getSpecificeBootcamp)
  .put(updateBootcamp)
  .delete(deleteBootcamp);

module.exports = router;
