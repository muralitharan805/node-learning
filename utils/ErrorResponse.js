class ErrorResponse extends Error {
  constructor(message, stats_code) {
    super(message);
    this.stats_code = stats_code;
    this.message = message;
  }
}
module.exports = ErrorResponse;
