const mqtt = require("mqtt");

exports.mqttSubcriber = () => {
  // MQTT client
  const mqttOptions = {
    host: "localhost", // MQTT broker host
    port: 1883, // MQTT broker port
  };
  // MQTT broker connection options
  const mqttClient = mqtt.connect(mqttOptions);

  // Subscribe to all topics
  mqttClient.on("connect", () => {
    mqttClient.subscribe("#", (err) => {
      if (err) {
        console.error("Failed to subscribe:", err);
      } else {
        console.log("Subscribed to all topics");
      }
    });
  });

  // Handle incoming messages
  mqttClient.on("message", (topic, message) => {
    const data = {
      topic: topic,
      message: message.toString(),
      timestamp: new Date(),
    };

    console.log("Data inserted into MongoDB:", data);
  });
};
