const express = require("express");
const dotenv = require("dotenv");
dotenv.config({ path: "./config/config.env" });
const app = express();
const bootcamp = require("./routes/bootcamp-routes");
const { connectDB } = require("./db");
const handleError = require("./middleware/error");
const courseRoutes = require("./routes/course.routes");
connectDB();
app.use(express.json());
app.use("/api/v1/bootcamp", bootcamp);
app.use("/api/v1/course", courseRoutes);
app.get("/", (req, res) => {
  res.send("test");
});
app.use(handleError);
app.listen(process.env.PORT, () => {
  console.log("server is started " + process.env.PORT);
});
process.on("unhandledRejection", (error) => {
  console.log("testt");
  console.log(error);
});
