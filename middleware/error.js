const ErrorResponse = require("../utils/ErrorResponse");

module.exports = handleError = (error, request, response, next) => {
  console.log("error middleware ", error.errors);
  if (error.name === "ValidationError") {
    const message = Object.values(error.errors).map((value) => value.message);
    error = new ErrorResponse(message, 400);
    // console.log(message);
    // return next(); // Return ErrorResponse
  }

  response.status(error.status_code || 500).json({
    // Adjust to use status_code instead of stats_code
    success: false,
    error: error.message || "Server Error",
  });
};
