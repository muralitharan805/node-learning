const mongoose = require("mongoose");
const { mqttSubcriber } = require("./utils/mqttSubcriber");

exports.connectDB = async () => {
  await mongoose.connect(process.env.DB_STRING).then((data) => {
    console.log(`database connected ${data.connection.host}`);
    // mqttSubcriber();
  });
};
