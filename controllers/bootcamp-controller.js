const bootcampModel = require("../model/bootcamp-model");
const ErrorResponse = require("../utils/ErrorResponse");
exports.getAllBootcamp = async (request, response, next) => {
  try {
    const getBootcampData = await bootcampModel.find();
    response.status(200).json({
      success: true,
      message: "show all bootcamp",
      data: getBootcampData,
    });
  } catch (error) {
    next(error);
  }
};
exports.getSpecificeBootcamp = async (request, response, next) => {
  try {
    const getBootcampData = await bootcampModel.findById(request.params.id);
    response.status(200).json({
      success: true,
      message: "show all bootcamp",
      data: getBootcampData,
    });
  } catch (error) {
    next(new ErrorResponse("data not found", 404));
  }
};
exports.createBootcamp = async (request, response, next) => {
  console.log(request.body);
  try {
    const createBootcampData = await bootcampModel.create(request.body);
    response.status(201).json({
      success: true,
      message: `create bootcamp`,
      data: createBootcampData,
    });
  } catch (error) {
    next(error);

    // const message = Object.values(error.errors).map((value) => value.message);

    // response.status(201).json({
    //   success: false,
    //   message: `create bootcamp`,
    //   data: message,
    // });
  }
};
exports.updateBootcamp = async (request, response, next) => {
  try {
    const updateBootcampData = await bootcampModel.findByIdAndUpdate(
      request.params.id,
      request.body,
      {
        new: true,
        runValidators: true,
      }
    );
    response.status(200).json({
      success: true,
      message: "data updated",
      data: updateBootcampData,
    });
  } catch (error) {
    next(error);
  }
};
exports.deleteBootcamp = async (request, response, next) => {
  try {
    const deleteBootcampData = await bootcampModel.findByIdAndDelete(
      request.params.id
    );
    response.status(200).json({
      success: true,
      message: "data deleted",
      data: deleteBootcampData,
    });
  } catch (error) {
    next(error);
  }
};
