const courseModel = require("../model/course-model");

exports.getAllCourse = async (request, response, next) => {
  try {
    const fetch_courses = await courseModel.find().populate("bootcamp");
    response.status(200).json({
      success: true,
      message: "show all course data",
      data: fetch_courses,
    });
  } catch (error) {
    next(error);
  }
};

exports.createCourse = async (request, response, next) => {
  try {
    const create_courses = await courseModel.create(request.body);
    response.status(200).json({
      success: true,
      message: "show all course data",
      data: create_courses,
    });
  } catch (error) {
    next(error);
  }
};
